<?php

namespace App\Repository;

use App\Entity\EquipeEducative;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EquipeEducative|null find($id, $lockMode = null, $lockVersion = null)
 * @method EquipeEducative|null findOneBy(array $criteria, array $orderBy = null)
 * @method EquipeEducative[]    findAll()
 * @method EquipeEducative[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipeEducativeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipeEducative::class);
    }

    // /**
    //  * @return EquipeEducative[] Returns an array of EquipeEducative objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EquipeEducative
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
