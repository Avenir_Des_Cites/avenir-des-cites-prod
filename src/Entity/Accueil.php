<?php

namespace App\Entity;

use App\Repository\AccueilRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=AccueilRepository::class)
 * @Vich\Uploadable
 */
class Accueil
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    
     /**
     * @Vich\UploadableField(mapping="accueil_logo", fileNameProperty="logo")
     */
    
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $encartTitre;

    /**
     * @ORM\Column(type="text")
     */
    private $encartText;

    /**
     * @ORM\Column(type="text")
     */
    private $siegeSocial;

     /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getEncartTitre(): ?string
    {
        return $this->encartTitre;
    }

    public function setEncartTitre(string $encartTitre): self
    {
        $this->encartTitre = $encartTitre;

        return $this;
    }

    public function getEncartText(): ?string
    {
        return $this->encartText;
    }

    public function setEncartText(string $encartText): self
    {
        $this->encartText = $encartText;

        return $this;
    }

    public function getSiegeSocial(): ?string
    {
        return $this->siegeSocial;
    }

    public function setSiegeSocial(string $siegeSocial): self
    {
        $this->siegeSocial = $siegeSocial;

        return $this;
    }

    /**
     * Get the value of logoFile
     * 
     * @return File
     */ 
    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    /**
     * Set the value of logoFile
     * 
     * @param  File  $logoFile
     *
     * @return  self
     */ 
    public function setLogoFile(?File $logoFile)
    {
        $this->logoFile = $logoFile;

        if (null !== $logoFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }
}
