<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\PartenairesRepository;
use App\Repository\EquipeEducativeRepository;
use App\Repository\PresentationRepository;
use App\Repository\AccueilRepository;
use App\Repository\EventsRepository;
use App\Repository\SocialNetworkRepository;
use App\Controller\ArticleController;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"}): Response
     */
    public function index(AccueilRepository $accueilRepository, EquipeEducativeRepository $equipeRepository, SocialNetworkRepository $socialNetworkRepository)
    {
        return $this->render('home/index.html.twig', [
            'accueil' => $accueilRepository->findAll(),
            'equipe' => $equipeRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }

    /**
     * @Route("/header", name="header", methods={"GET"}): Response
     */
    public function header(AccueilRepository $accueilRepository, SocialNetworkRepository $socialNetworkRepository)
    {
        return $this->render('partials/header.html.twig', [
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }

    /**
     * @Route("/footer", name="footer", methods={"GET"}): Response
     */
    public function footer(AccueilRepository $accueilRepository, SocialNetworkRepository $socialNetworkRepository)
    {
        return $this->render('partials/header.html.twig', [
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }

    /**
     * @Route("/partenaires", name="partenaires", methods={"GET"})
     */
    public function partenaires(PartenairesRepository $partenaireRepository, AccueilRepository $accueilRepository, SocialNetworkRepository $socialNetworkRepository): Response
    {
        return $this->render('home/partenaires.html.twig', [
            'partenaires' => $partenaireRepository->findAll(),
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }

    /**
     * @Route("/equipe", name="equipe", methods={"GET"})
     */
    public function equipe(EquipeEducativeRepository $equipeRepository, AccueilRepository $accueilRepository, SocialNetworkRepository $socialNetworkRepository): Response
    {
        return $this->render('home/equipe.html.twig', [
            'equipe' => $equipeRepository->findAll(),
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }

    /**
     * @Route("/presentation", name="presentation", methods={"GET"})
     */
    public function presentation(PresentationRepository $presentationRepository, AccueilRepository $accueilRepository, SocialNetworkRepository $socialNetworkRepository): Response
    {
        return $this->render('home/presentation.html.twig', [
            'presentation' => $presentationRepository->findAll(),
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }
    
    /**
     * @Route("/articles", name="article_index", methods={"GET"})
     */
    public function articles(ArticleRepository $articleRepository, AccueilRepository $accueilRepository, EventsRepository $eventsRepository, SocialNetworkRepository $socialNetworkRepository, PaginatorInterface $paginator, EntityManagerInterface $em, Request $request): Response
    {
        $dql   = "SELECT a FROM App:Article a";
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            5 /*limit per page*/
        );

        return $this->render('home/articles.html.twig', [
            'pagination' => $pagination,
            'articles' => $articleRepository->findAll(),
            'events' => $eventsRepository->findEvent(),
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }

    /**
     * @Route("/mentions-legales", name="mentions", methods={"GET"})
     */
    public function mentions(AccueilRepository $accueilRepository, SocialNetworkRepository $socialNetworkRepository)
    {
        return $this->render('home/mentionslegales.html.twig', [
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="article_show",requirements={"id"="\d+"}, methods={"GET"})
     */
    public function article(Article $article, AccueilRepository $accueilRepository, SocialNetworkRepository $socialNetworkRepository): Response
    {
        return $this->render('home/article.html.twig', [
            'article' => $article,
            'accueil' => $accueilRepository->findAll(),
            'reseaux' => $socialNetworkRepository->findAll(),
        ]);
    }
     
}
