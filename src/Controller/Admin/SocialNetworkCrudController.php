<?php

namespace App\Controller\Admin;

use App\Entity\SocialNetwork;
use App\Repository\SocialNetworkRepository;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;


class SocialNetworkCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SocialNetwork::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('un réseau social')
            // the visible title at the top of the page and the content of the <title> element
            // it can include these placeholders: %entity_id%, %entity_label_singular%, %entity_label_plural%
            ->setPageTitle('index', 'Réseaux Sociaux')
            ->setPageTitle('new', 'Réseau social')
            ->setPageTitle('edit', 'Réseau social')
        ;
    }
    public function configureFields(string $pageName): iterable
    {
        return [
            //affiche l'ID mais empêche sa modification
            // IdField::new('id')->hideOnForm(),
            // permet de définir les champs d'administration et leur label en mode édition et création
            TextField::new('name', 'Nom'),
            UrlField::new('url'),
            ImageField::new('logoFile')
            ->setFormType(VichImageType::class)
            ->setLabel('Logo'), 
        ];
    }
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
