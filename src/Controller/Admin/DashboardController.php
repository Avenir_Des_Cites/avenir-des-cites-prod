<?php

namespace App\Controller\Admin;

use App\Entity\Accueil;
use App\Entity\Admin;
use App\Entity\Article;
use App\Entity\EquipeEducative;
use App\Entity\Events;
use App\Entity\Partenaires;
use App\Entity\Presentation;
use App\Entity\SocialNetwork;

use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
          // redirect to some CRUD controller
          $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

          return $this->redirect($routeBuilder->setController(ArticleCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Avenir Des Cités');
    }

    public function configureMenuItems(): iterable
    {
        return [

            yield MenuItem::section('Accueil'),
            yield MenuItem::linkToCrud('Accueil', 'fas fa-home', Accueil::class),

            yield MenuItem::section('Blog'),
            yield MenuItem::linkToDashboard('Articles', 'far fa-newspaper'),
            yield MenuItem::linkToCrud('Créer un nouvel article', 'fas fa-angle-double-right', Article::class)
            ->setAction('new'),

            yield MenuItem::section('Présentation'),
            yield MenuItem::linkToCrud('Présentation', 'far fa-bookmark', Presentation::class),

            yield MenuItem::section('Partenaires'),
            yield MenuItem::linkToCrud('Partenaires', 'fas fa-handshake', Partenaires::class),

            yield MenuItem::section('Équipe'),
            yield MenuItem::linkToCrud('Équipe éducative', 'fas fa-users', EquipeEducative::class),

            yield MenuItem::section('Réseaux sociaux'),
            yield MenuItem::linkToCrud('Réseaux sociaux', 'fas fa-comment-alt', SocialNetwork::class),
            
            yield MenuItem::section('Évènements'),
            yield MenuItem::linkToCrud('Évènements', 'fas fa-calendar', Events::class),

        ];
    }
}
