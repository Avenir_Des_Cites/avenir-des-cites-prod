<?php

namespace App\Controller\Admin;

use App\Entity\Partenaires;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PartenairesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Partenaires::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('un partenaire')
        ;
    }

    public function configureFields(string $pageName): iterable
{
    return [
        //affiche l'ID mais empêche sa modification
        IdField::new('id')
        ->hideOnForm()
        ->hideOnIndex(),
        // permet de définir les champs d'administration et leur label en mode édition et création
        TextField::new('name', 'Nom'),
        ImageField::new('logoFile')
        ->setFormType(VichImagetype::class)
        ->setLabel('Logo')
        ->hideOnIndex(),
    ];
}
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
