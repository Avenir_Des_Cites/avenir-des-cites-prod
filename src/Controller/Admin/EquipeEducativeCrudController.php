<?php

namespace App\Controller\Admin;

use App\Entity\EquipeEducative;
use App\Repository\EquipeEducativeRepository;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EquipeEducativeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EquipeEducative::class;
    }

    public function configureCrud(Crud $crud): Crud
{
    return $crud
        ->setEntityLabelInSingular('un membre')
        // the visible title at the top of the page and the content of the <title> element
        // it can include these placeholders: %entity_id%, %entity_label_singular%, %entity_label_plural%
        ->setPageTitle('index', 'Équipe éducative')
        ->setPageTitle('new', 'Équipe éducative')
        ->setPageTitle('edit', 'Équipe éducative')
    ;
}
public function configureFields(string $pageName): iterable
{
    return [
        //affiche l'ID mais empêche sa modification
        // IdField::new('id')->hideOnForm(),
        // permet de définir les champs d'administration et leur label en mode édition et création
        TextField::new('lastName', 'Nom'),
        TextField::new('firstName', 'Prénom'),
        TelephoneField::new('phoneNumber', 'Numéro de téléphone'),
        EmailField::new('mail', 'Adresse mail'),
        TextField::new('city', 'Villes d\'interventions'),
        ImageField::new('photoFile')
        ->setFormType(VichImageType::class)
        ->setLabel('Photo')
        ->hideOnIndex(),
        TextField::new('role', 'Poste')
    ];
}

    // public function configureFields(string $pageName): iterable
    // {
    //     return [
    //         IdField::new('id'),
    //         TextField::new('title'),
    //         TextEditorField::new('description'),
    //     ];
    // }
}
