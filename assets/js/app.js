/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../scss/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';
const $ = require('jquery');
require('bootstrap');
// Import Bootstrap JS
require('bootstrap-star-rating');

// // EventListener Schéma HomePage
// let blockNode = document.querySelectorAll(".intervention__description");
// let pictoNode = document.querySelectorAll(".picto");

// function getVisible(block){
//         block.classList.toggle("visible");
// }

// for (let i = 0; i < pictoNode.length; i++) {
//     pictoNode[i].addEventListener("mouseover", () => getVisible(blockNode[i]));
//     pictoNode[i].addEventListener("mouseout", () => getVisible(blockNode[i]));
    
// }
// //

