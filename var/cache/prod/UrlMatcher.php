<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\Admin\\DashboardController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'], null, ['GET' => 0], null, false, false, null]],
        '/header' => [[['_route' => 'header', '_controller' => 'App\\Controller\\HomeController::header'], null, ['GET' => 0], null, false, false, null]],
        '/footer' => [[['_route' => 'footer', '_controller' => 'App\\Controller\\HomeController::footer'], null, ['GET' => 0], null, false, false, null]],
        '/partenaires' => [[['_route' => 'partenaires', '_controller' => 'App\\Controller\\HomeController::partenaires'], null, ['GET' => 0], null, false, false, null]],
        '/equipe' => [[['_route' => 'equipe', '_controller' => 'App\\Controller\\HomeController::equipe'], null, ['GET' => 0], null, false, false, null]],
        '/presentation' => [[['_route' => 'presentation', '_controller' => 'App\\Controller\\HomeController::presentation'], null, ['GET' => 0], null, false, false, null]],
        '/articles' => [[['_route' => 'article_index', '_controller' => 'App\\Controller\\HomeController::articles'], null, ['GET' => 0], null, false, false, null]],
        '/mentions-legales' => [[['_route' => 'mentions', '_controller' => 'App\\Controller\\HomeController::mentions'], null, ['GET' => 0], null, false, false, null]],
        '/reset-password' => [[['_route' => 'app_forgot_password_request', '_controller' => 'App\\Controller\\ResetPasswordController::request'], null, null, null, false, false, null]],
        '/reset-password/check-email' => [[['_route' => 'app_check_email', '_controller' => 'App\\Controller\\ResetPasswordController::checkEmail'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/(\\d+)(*:13)'
                .'|/reset\\-password/reset(?:/([^/]++))?(*:56)'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        13 => [[['_route' => 'article_show', '_controller' => 'App\\Controller\\HomeController::article'], ['id'], ['GET' => 0], null, false, true, null]],
        56 => [
            [['_route' => 'app_reset_password', 'token' => null, '_controller' => 'App\\Controller\\ResetPasswordController::reset'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
