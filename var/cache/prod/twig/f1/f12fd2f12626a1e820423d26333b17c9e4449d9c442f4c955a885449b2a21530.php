<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/partenaires.html.twig */
class __TwigTemplate_acd25f80201a0d4d18d28a9f0e7c7458980ce98ee5ab0b6293ed88f52c697835 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/partenaires.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/partenaires.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Accueil";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    
    <div class=\"partenaires__image\">
    </div>
    <div class=\"container-header\"></div>
    <div class=\"container\">
        <div class=\"partenaires\">
            <h1 class=\"partenaires__title\">NOS PARTENAIRES</h1>
            <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["partenaires"]) || array_key_exists("partenaires", $context) ? $context["partenaires"] : (function () { throw new RuntimeError('Variable "partenaires" does not exist.', 13, $this->source); })()), 0, [], "array", false, false, false, 13), "logoFile"), "html", null, true);
        echo "\" class=\"logo__pdc\" alt=\"Logo du département du Pas-de-Calais\"/>
            <hr class=\"ligne__partenaires\"/>
            <h2 class=\"territoires\">LES TERRITOIRES D'INTERVENTION</h2>
            <div class=\"cities\">
                <img src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["partenaires"]) || array_key_exists("partenaires", $context) ? $context["partenaires"] : (function () { throw new RuntimeError('Variable "partenaires" does not exist.', 17, $this->source); })()), 1, [], "array", false, false, false, 17), "logoFile"), "html", null, true);
        echo "\" class=\"logo__harnes\" alt=\"Logo de la ville de Harnes\"/>
                <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["partenaires"]) || array_key_exists("partenaires", $context) ? $context["partenaires"] : (function () { throw new RuntimeError('Variable "partenaires" does not exist.', 18, $this->source); })()), 2, [], "array", false, false, false, 18), "logoFile"), "html", null, true);
        echo "\" class=\"logo__billymontigny\" alt=\"Logo de la ville de Billy-Montigny\"/>
                <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["partenaires"]) || array_key_exists("partenaires", $context) ? $context["partenaires"] : (function () { throw new RuntimeError('Variable "partenaires" does not exist.', 19, $this->source); })()), 3, [], "array", false, false, false, 19), "logoFile"), "html", null, true);
        echo "\" class=\"logo__sallaumines\" alt=\"Logo de la ville de Sallaumines\"/>
            </div>
            <hr class=\"ligne__partenaires\"/>
            <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["partenaires"]) || array_key_exists("partenaires", $context) ? $context["partenaires"] : (function () { throw new RuntimeError('Variable "partenaires" does not exist.', 22, $this->source); })()), 4, [], "array", false, false, false, 22), "logoFile"), "html", null, true);
        echo "\" class=\"logo__caf\" alt=\"Logo de la CAF du Pas-de-Calais\"/>
            <hr class=\"ligne__partenaires\"/>
            
            <div class=\"contact\">
                <p class=\"contact--vert mx-auto\">Vous souhaitez devenir partenaire de l’association ? <br/>N’hésitez pas à nous contacter:</p>
                <p class=\"contact__description\">03.61.93.11.38<br/>
                club.d.cites@gmail.com</p>
                <div class=\"contact__siege\">
                    ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["accueil"]);
        foreach ($context['_seq'] as $context["_key"] => $context["accueil"]) {
            // line 31
            echo "                    <p class=\"contact__siege\">";
            echo twig_get_attribute($this->env, $this->source, $context["accueil"], "siegeSocial", [], "any", false, false, false, 31);
            echo "</p>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['accueil'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                </div>
          </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/partenaires.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 33,  118 => 31,  114 => 30,  103 => 22,  97 => 19,  93 => 18,  89 => 17,  82 => 13,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Accueil{% endblock %}

{% block body %}
    
    <div class=\"partenaires__image\">
    </div>
    <div class=\"container-header\"></div>
    <div class=\"container\">
        <div class=\"partenaires\">
            <h1 class=\"partenaires__title\">NOS PARTENAIRES</h1>
            <img src=\"{{ vich_uploader_asset(partenaires[0], 'logoFile') }}\" class=\"logo__pdc\" alt=\"Logo du département du Pas-de-Calais\"/>
            <hr class=\"ligne__partenaires\"/>
            <h2 class=\"territoires\">LES TERRITOIRES D'INTERVENTION</h2>
            <div class=\"cities\">
                <img src=\"{{ vich_uploader_asset(partenaires[1], 'logoFile') }}\" class=\"logo__harnes\" alt=\"Logo de la ville de Harnes\"/>
                <img src=\"{{ vich_uploader_asset(partenaires[2], 'logoFile') }}\" class=\"logo__billymontigny\" alt=\"Logo de la ville de Billy-Montigny\"/>
                <img src=\"{{ vich_uploader_asset(partenaires[3], 'logoFile') }}\" class=\"logo__sallaumines\" alt=\"Logo de la ville de Sallaumines\"/>
            </div>
            <hr class=\"ligne__partenaires\"/>
            <img src=\"{{ vich_uploader_asset(partenaires[4], 'logoFile') }}\" class=\"logo__caf\" alt=\"Logo de la CAF du Pas-de-Calais\"/>
            <hr class=\"ligne__partenaires\"/>
            
            <div class=\"contact\">
                <p class=\"contact--vert mx-auto\">Vous souhaitez devenir partenaire de l’association ? <br/>N’hésitez pas à nous contacter:</p>
                <p class=\"contact__description\">03.61.93.11.38<br/>
                club.d.cites@gmail.com</p>
                <div class=\"contact__siege\">
                    {% for accueil in accueil %}
                    <p class=\"contact__siege\">{{ accueil.siegeSocial | raw }}</p>
                    {% endfor %}
                </div>
          </div>
        </div>
    </div>

{% endblock %}
", "home/partenaires.html.twig", "/htdocs/templates/home/partenaires.html.twig");
    }
}
