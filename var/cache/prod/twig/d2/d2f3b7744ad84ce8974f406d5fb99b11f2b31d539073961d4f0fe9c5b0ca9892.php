<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/index.html.twig */
class __TwigTemplate_5b42e97fbd13886c5d9d8b99839265f0757cae16c1cbd96d2952225704f75224 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Accueil";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"presentation__image\">
        <div class=\"card\">
            <div class=\"card-body\">
            ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["accueil"]) || array_key_exists("accueil", $context) ? $context["accueil"] : (function () { throw new RuntimeError('Variable "accueil" does not exist.', 10, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 11
            echo "                <h5 class=\"card-title\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "encartTitre", [], "any", false, false, false, 11), "html", null, true);
            echo "</h5>
                <p class=\"card-text\">";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "encartText", [], "any", false, false, false, 12), "html", null, true);
            echo "</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "            </div>
        </div>
    </div>
    <div class=\"container-header\"></div>
    <div class=\"container\">
        <div class=\"directeurs\">
            <div class=\"directeur--left\">
                <img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 21, $this->source); })()), 6, [], "array", false, false, false, 21), "photoFile"), "html", null, true);
        echo "\" class=\"directeurs__image d-flex justify-content-center\" alt=\"Francis Gauthier\">
                <div class=\"directeur__text text-center\">
                    <h5 class=\"uppercase\">";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 23, $this->source); })()), 6, [], "array", false, false, false, 23), "firstname", [], "any", false, false, false, 23), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 23, $this->source); })()), 6, [], "array", false, false, false, 23), "lastname", [], "any", false, false, false, 23), "html", null, true);
        echo "</h5>
                    <h5 class=\"uppercase\">";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 24, $this->source); })()), 6, [], "array", false, false, false, 24), "role", [], "any", false, false, false, 24), "html", null, true);
        echo "</h5>
                </div>
            </div>
            <div class=\"directeur--right\">
                <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 28, $this->source); })()), 0, [], "array", false, false, false, 28), "photoFile"), "html", null, true);
        echo "\" class=\"directeurs__image d-flex justify-content-center\" alt=\"Sadek Deghima\">
                <div class=\"directeur__text text-center\">
                    <h5 class=\"uppercase\">";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 30, $this->source); })()), 0, [], "array", false, false, false, 30), "firstname", [], "any", false, false, false, 30), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 30, $this->source); })()), 0, [], "array", false, false, false, 30), "lastname", [], "any", false, false, false, 30), "html", null, true);
        echo "</h5>
                    <h5 class=\"uppercase\">";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 31, $this->source); })()), 0, [], "array", false, false, false, 31), "role", [], "any", false, false, false, 31), "html", null, true);
        echo "</h5>
                </div>
            </div>
        </div>
        <div class=\"equipe d-flex justify-content-center\">
            <a href=\"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("equipe");
        echo "\" class=\"btn equipe__button equipe__button__link\">Découvrez le reste de notre équipe</a>
        </div>
    </div>
    <div class=\"container__schema\">
        <div class=\"container\">
            <p class=\"preschema\">La prévention spécialisée est une action éducative et sociale qui consiste à aller vers le jeune dans son milieu de vie. L’objectif est d’assurer une continuité éducative auprès du jeune qu’il soit dans sa famille à l’école ou dans la rue.</p>
        </div>
        <h4 class=\"interventions__title\">Les modes d’intervention:</h4>
        <div class=\"interventions\">
            
            <div class=\"interventions--left\">
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--up--left\">
                    <div class=\"picto\">
                        <img src=\"img/house_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme maison\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">TRAVAIL DE RUE</h6>
                        <p class=\"intervention__text\">Présence dans le quartier et aux abords des collèges</p>
                    </div>
                </div>
                <div class=\"intervention intervention--left--secondchild\">
                    <hr class=\"ligne ligne--middle--left\">
                    <div class=\"picto\">
                        <img src=\"img/velos_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme velos\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ACTIONS COLLECTIVES</h6>
                        <p class=\"intervention__text\">Sport, culture, loisirs (tournois sportifs, sorties VTT, atelier danse...)</p>
                    </div>
                </div>
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--down--left\">
                    <div class=\"picto\">
                        <img src=\"img/mobile_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme support numérique\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ANIMATIONS NUMERIQUES</h6>
                        <p class=\"intervention__text\">Education aux médias, sérious game, prévention, ateliers tablettes</p>
                    </div>
                </div>
            </div>
            <div class=\"interventions__logo\">
                <img src=\"img/A_icon.svg\" width=\"80\" height=\"80\" alt=\"Logo d'Avenir des Cités\">
                <p>Avenir des Cités</p>
            </div>
            <div class=\"interventions--right\">
                <div class=\"intervention intervention--firstchild\">
                    <hr class=\"ligne ligne--up--right\">
                    <div class=\"picto\">
                        <img src=\"img/boussole_circle.svg\" class=\"intervention__picto intervention__picto--boussole\" width=\"60\" height=\"60\" alt=\"Pictogramme boussole\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ORIENTATION</h6>
                        <p class=\"intervention__text\">Travail en réseau et en partenariat</p>
                    </div>
                </div>
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--middle--right\">
                    <div class=\"picto\">
                        <img src=\"img/infos_circle.svg\" class=\"intervention__picto intervention__picto--info\" width=\"60\" height=\"60\" alt=\"Pictogramme information\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ACCUEIL / INFORMATION</h6>
                        <p class=\"intervention__text\">Accès aux droits, travail, santé, logement, démarches administratives, éducation, parentalité</p>
                    </div>
                </div>
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--down--right\">
                    <div class=\"picto\">
                        <img src=\"img/talks_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme parole\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">AIDES AUX PROJETS ET ACCOMPAGNEMENT SOCIAL</h6>
                        <p class=\"intervention__text\">Individuel, collectif, insertion sociale &amp; professionnelle</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"container\">
        <ul class=\"liste mx-auto\">
            <li class=\"puces\">Libre adhésion</li>
            <li class=\"puces\">Anonymat</li>
            <li class=\"puces\">Confidentialité</li>
            <li class=\"puces\">Non mandat</li>
        </ul>
        <p class=\"pre-objectifs pb-5\">La prévention spécialisée est destinée à faire face à des situations de fragilité affective, de rupture par rapport à l’environnement social et familial.</p>
        <div class=\"objectifs\">
            <h4 class=\"objectifs__title\">Les objectifs de cette action éducative sont de :</h4>
            <div class=\"objectifs__args pb-5\">
                <div class=\"objectifs__arg\"><strong>Prévenir les risques d’exclusion</strong> en favorisant l’accès aux droits, à l’éducation, à la santé, à la culture et aux sports.</div>
                <div class=\"objectifs__arg\"><strong>Prévenir les conduites à risques</strong> qui peuvent être liées à des fragilités individuelles, à la dureté de certains contextes sociaux et urbains, à des violences subies.</div>
                <div class=\"objectifs__arg--last\"><strong>Aider à un meilleur dialogue entre jeunes et adultes</strong> et contribuer à favoriser l’émergence de réseaux de solidarités locales à partir des potentialités du milieu.</div>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 36,  134 => 31,  128 => 30,  123 => 28,  116 => 24,  110 => 23,  105 => 21,  96 => 14,  88 => 12,  83 => 11,  79 => 10,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Accueil{% endblock %}

{% block body %}

    <div class=\"presentation__image\">
        <div class=\"card\">
            <div class=\"card-body\">
            {% for item in accueil %}
                <h5 class=\"card-title\">{{ item.encartTitre }}</h5>
                <p class=\"card-text\">{{ item.encartText }}</p>
                {% endfor %}
            </div>
        </div>
    </div>
    <div class=\"container-header\"></div>
    <div class=\"container\">
        <div class=\"directeurs\">
            <div class=\"directeur--left\">
                <img src=\"{{ vich_uploader_asset(equipe[6], 'photoFile') }}\" class=\"directeurs__image d-flex justify-content-center\" alt=\"Francis Gauthier\">
                <div class=\"directeur__text text-center\">
                    <h5 class=\"uppercase\">{{ equipe[6].firstname}} {{equipe[6].lastname}}</h5>
                    <h5 class=\"uppercase\">{{ equipe[6].role}}</h5>
                </div>
            </div>
            <div class=\"directeur--right\">
                <img src=\"{{ vich_uploader_asset(equipe[0], 'photoFile') }}\" class=\"directeurs__image d-flex justify-content-center\" alt=\"Sadek Deghima\">
                <div class=\"directeur__text text-center\">
                    <h5 class=\"uppercase\">{{ equipe[0].firstname}} {{equipe[0].lastname}}</h5>
                    <h5 class=\"uppercase\">{{ equipe[0].role}}</h5>
                </div>
            </div>
        </div>
        <div class=\"equipe d-flex justify-content-center\">
            <a href=\"{{ path('equipe') }}\" class=\"btn equipe__button equipe__button__link\">Découvrez le reste de notre équipe</a>
        </div>
    </div>
    <div class=\"container__schema\">
        <div class=\"container\">
            <p class=\"preschema\">La prévention spécialisée est une action éducative et sociale qui consiste à aller vers le jeune dans son milieu de vie. L’objectif est d’assurer une continuité éducative auprès du jeune qu’il soit dans sa famille à l’école ou dans la rue.</p>
        </div>
        <h4 class=\"interventions__title\">Les modes d’intervention:</h4>
        <div class=\"interventions\">
            
            <div class=\"interventions--left\">
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--up--left\">
                    <div class=\"picto\">
                        <img src=\"img/house_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme maison\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">TRAVAIL DE RUE</h6>
                        <p class=\"intervention__text\">Présence dans le quartier et aux abords des collèges</p>
                    </div>
                </div>
                <div class=\"intervention intervention--left--secondchild\">
                    <hr class=\"ligne ligne--middle--left\">
                    <div class=\"picto\">
                        <img src=\"img/velos_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme velos\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ACTIONS COLLECTIVES</h6>
                        <p class=\"intervention__text\">Sport, culture, loisirs (tournois sportifs, sorties VTT, atelier danse...)</p>
                    </div>
                </div>
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--down--left\">
                    <div class=\"picto\">
                        <img src=\"img/mobile_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme support numérique\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ANIMATIONS NUMERIQUES</h6>
                        <p class=\"intervention__text\">Education aux médias, sérious game, prévention, ateliers tablettes</p>
                    </div>
                </div>
            </div>
            <div class=\"interventions__logo\">
                <img src=\"img/A_icon.svg\" width=\"80\" height=\"80\" alt=\"Logo d'Avenir des Cités\">
                <p>Avenir des Cités</p>
            </div>
            <div class=\"interventions--right\">
                <div class=\"intervention intervention--firstchild\">
                    <hr class=\"ligne ligne--up--right\">
                    <div class=\"picto\">
                        <img src=\"img/boussole_circle.svg\" class=\"intervention__picto intervention__picto--boussole\" width=\"60\" height=\"60\" alt=\"Pictogramme boussole\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ORIENTATION</h6>
                        <p class=\"intervention__text\">Travail en réseau et en partenariat</p>
                    </div>
                </div>
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--middle--right\">
                    <div class=\"picto\">
                        <img src=\"img/infos_circle.svg\" class=\"intervention__picto intervention__picto--info\" width=\"60\" height=\"60\" alt=\"Pictogramme information\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">ACCUEIL / INFORMATION</h6>
                        <p class=\"intervention__text\">Accès aux droits, travail, santé, logement, démarches administratives, éducation, parentalité</p>
                    </div>
                </div>
                <div class=\"intervention\">
                    <hr class=\"ligne ligne--down--right\">
                    <div class=\"picto\">
                        <img src=\"img/talks_circle.svg\" class=\"intervention__picto\" width=\"60\" height=\"60\" alt=\"Pictogramme parole\">
                    </div>
                    <div class=\"intervention__description\">
                        <h6 class=\"intervention__title\">AIDES AUX PROJETS ET ACCOMPAGNEMENT SOCIAL</h6>
                        <p class=\"intervention__text\">Individuel, collectif, insertion sociale &amp; professionnelle</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"container\">
        <ul class=\"liste mx-auto\">
            <li class=\"puces\">Libre adhésion</li>
            <li class=\"puces\">Anonymat</li>
            <li class=\"puces\">Confidentialité</li>
            <li class=\"puces\">Non mandat</li>
        </ul>
        <p class=\"pre-objectifs pb-5\">La prévention spécialisée est destinée à faire face à des situations de fragilité affective, de rupture par rapport à l’environnement social et familial.</p>
        <div class=\"objectifs\">
            <h4 class=\"objectifs__title\">Les objectifs de cette action éducative sont de :</h4>
            <div class=\"objectifs__args pb-5\">
                <div class=\"objectifs__arg\"><strong>Prévenir les risques d’exclusion</strong> en favorisant l’accès aux droits, à l’éducation, à la santé, à la culture et aux sports.</div>
                <div class=\"objectifs__arg\"><strong>Prévenir les conduites à risques</strong> qui peuvent être liées à des fragilités individuelles, à la dureté de certains contextes sociaux et urbains, à des violences subies.</div>
                <div class=\"objectifs__arg--last\"><strong>Aider à un meilleur dialogue entre jeunes et adultes</strong> et contribuer à favoriser l’émergence de réseaux de solidarités locales à partir des potentialités du milieu.</div>
            </div>
        </div>
    </div>

{% endblock %}
", "home/index.html.twig", "/htdocs/templates/home/index.html.twig");
    }
}
