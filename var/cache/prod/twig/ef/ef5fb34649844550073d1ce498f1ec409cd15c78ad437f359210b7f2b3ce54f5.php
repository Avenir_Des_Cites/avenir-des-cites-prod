<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/equipe.html.twig */
class __TwigTemplate_23702e463e8c970be89b426ead99d47b33fb70733232b198ba8ed97ede869b66 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/equipe.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/equipe.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Equipe";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"container\">
        <div class=\"responsable\">
            <h1 class=\"responsable__title\">RESPONSABLE DE SERVICE</h1>
            <div class=\"equipe\">
                <div class=\"equipe__content\">
                    <div class=\"equipe__image\">
                        <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 13, $this->source); })()), 0, [], "array", false, false, false, 13), "photoFile"), "html", null, true);
        echo "\" class=\"equipe__img equipe__img--sadek\" alt=\"Sadek DEGHIMA\">
                    </div>
                    <div class=\"equipe__description\">
                        <h4 class=\"equipe__name\">";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 16, $this->source); })()), 0, [], "array", false, false, false, 16), "firstname", [], "any", false, false, false, 16), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 16, $this->source); })()), 0, [], "array", false, false, false, 16), "lastname", [], "any", false, false, false, 16), "html", null, true);
        echo "</h4>
                        <p>";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 17, $this->source); })()), 0, [], "array", false, false, false, 17), "phonenumber", [], "any", false, false, false, 17), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 17, $this->source); })()), 0, [], "array", false, false, false, 17), "mail", [], "any", false, false, false, 17), "html", null, true);
        echo "</p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"equipe-educative\">
            <h2 class=\"equipe-educative__title text-center\">EQUIPE EDUCATIVE</h2>
            <div class=\"equipe-educative__content\">
                <div class=\"harnes\">
                    <h3 class=\"harnes__title\">HARNES</h3>
                    <div class=\"harnes__content\">
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 31, $this->source); })()), 1, [], "array", false, false, false, 31), "photoFile"), "html", null, true);
        echo "\" class=\"equipe__img\" alt=\"Myriam DERROUICHE\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4 class=\"equipe__name\">";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 34, $this->source); })()), 1, [], "array", false, false, false, 34), "firstname", [], "any", false, false, false, 34), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 34, $this->source); })()), 1, [], "array", false, false, false, 34), "lastname", [], "any", false, false, false, 34), "html", null, true);
        echo "</h4>
                                    <p>";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 35, $this->source); })()), 1, [], "array", false, false, false, 35), "phonenumber", [], "any", false, false, false, 35), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 35, $this->source); })()), 1, [], "array", false, false, false, 35), "mail", [], "any", false, false, false, 35), "html", null, true);
        echo "</p>
                                </div>
                            </div>
                        </div>
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 42, $this->source); })()), 2, [], "array", false, false, false, 42), "photoFile"), "html", null, true);
        echo "\" class=\"equipe__img equipe__img--nordine\" alt=\"Nordine LAGRAGUI\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4 class=\"equipe__name\">";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 45, $this->source); })()), 2, [], "array", false, false, false, 45), "firstname", [], "any", false, false, false, 45), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 45, $this->source); })()), 2, [], "array", false, false, false, 45), "lastname", [], "any", false, false, false, 45), "html", null, true);
        echo "</h4>
                                    <p>";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 46, $this->source); })()), 2, [], "array", false, false, false, 46), "phonenumber", [], "any", false, false, false, 46), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 46, $this->source); })()), 2, [], "array", false, false, false, 46), "mail", [], "any", false, false, false, 46), "html", null, true);
        echo "</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"billy-sallaumines\">
                    <h3 class=\"billy-sallaumines__title align-items-baseline\">BILLY-MONTIGNY / SALLAUMINES</h3>
                    <div class=\"billy-sallaumines__content\">
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 58, $this->source); })()), 3, [], "array", false, false, false, 58), "photoFile"), "html", null, true);
        echo "\" class=\"equipe__img\" alt=\"Yohan SARDO\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4  class=\"equipe__name\">";
        // line 61
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 61, $this->source); })()), 3, [], "array", false, false, false, 61), "firstname", [], "any", false, false, false, 61), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 61, $this->source); })()), 3, [], "array", false, false, false, 61), "lastname", [], "any", false, false, false, 61), "html", null, true);
        echo "</h4>
                                    <p>";
        // line 62
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 62, $this->source); })()), 3, [], "array", false, false, false, 62), "phonenumber", [], "any", false, false, false, 62), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 62, $this->source); })()), 3, [], "array", false, false, false, 62), "mail", [], "any", false, false, false, 62), "html", null, true);
        echo "</p>
                                </div>
                            </div>
                        </div>
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 69, $this->source); })()), 4, [], "array", false, false, false, 69), "photoFile"), "html", null, true);
        echo "\" class=\"equipe__img\" alt=\"Xavier HEINTZE\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4 class=\"equipe__name\">";
        // line 72
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 72, $this->source); })()), 4, [], "array", false, false, false, 72), "firstname", [], "any", false, false, false, 72), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 72, $this->source); })()), 4, [], "array", false, false, false, 72), "lastname", [], "any", false, false, false, 72), "html", null, true);
        echo "</h4>
                                    <p>";
        // line 73
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 73, $this->source); })()), 4, [], "array", false, false, false, 73), "phonenumber", [], "any", false, false, false, 73), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 73, $this->source); })()), 4, [], "array", false, false, false, 73), "mail", [], "any", false, false, false, 73), "html", null, true);
        echo "</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"secretaire\">
            <h2 class=\"secretaire__title\">SECRETAIRE COMPTABLE</h2>
            <div class=\"equipe\">
                <div class=\"equipe__content\">
                    <div class=\"equipe__image\">
                        <img src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 86, $this->source); })()), 5, [], "array", false, false, false, 86), "photoFile"), "html", null, true);
        echo "\" class=\"equipe__img\" alt=\"Rabia BELAICHE\">
                    </div>
                    <div class=\"equipe__description\">
                        <h4 class=\"equipe__name\">";
        // line 89
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 89, $this->source); })()), 5, [], "array", false, false, false, 89), "firstname", [], "any", false, false, false, 89), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 89, $this->source); })()), 5, [], "array", false, false, false, 89), "lastname", [], "any", false, false, false, 89), "html", null, true);
        echo "</h4>
                        <p>";
        // line 90
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 90, $this->source); })()), 5, [], "array", false, false, false, 90), "phonenumber", [], "any", false, false, false, 90), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["equipe"]) || array_key_exists("equipe", $context) ? $context["equipe"] : (function () { throw new RuntimeError('Variable "equipe" does not exist.', 90, $this->source); })()), 5, [], "array", false, false, false, 90), "mail", [], "any", false, false, false, 90), "html", null, true);
        echo "</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/equipe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 90,  226 => 89,  220 => 86,  202 => 73,  196 => 72,  190 => 69,  178 => 62,  172 => 61,  166 => 58,  149 => 46,  143 => 45,  137 => 42,  125 => 35,  119 => 34,  113 => 31,  94 => 17,  88 => 16,  82 => 13,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Equipe{% endblock %}

{% block body %}

    <div class=\"container\">
        <div class=\"responsable\">
            <h1 class=\"responsable__title\">RESPONSABLE DE SERVICE</h1>
            <div class=\"equipe\">
                <div class=\"equipe__content\">
                    <div class=\"equipe__image\">
                        <img src=\"{{ vich_uploader_asset(equipe[0], 'photoFile') }}\" class=\"equipe__img equipe__img--sadek\" alt=\"Sadek DEGHIMA\">
                    </div>
                    <div class=\"equipe__description\">
                        <h4 class=\"equipe__name\">{{ equipe[0].firstname}} {{equipe[0].lastname}}</h4>
                        <p>{{ equipe[0].phonenumber }}<br>{{ equipe[0].mail }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"equipe-educative\">
            <h2 class=\"equipe-educative__title text-center\">EQUIPE EDUCATIVE</h2>
            <div class=\"equipe-educative__content\">
                <div class=\"harnes\">
                    <h3 class=\"harnes__title\">HARNES</h3>
                    <div class=\"harnes__content\">
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"{{ vich_uploader_asset(equipe[1], 'photoFile') }}\" class=\"equipe__img\" alt=\"Myriam DERROUICHE\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4 class=\"equipe__name\">{{ equipe[1].firstname}} {{equipe[1].lastname}}</h4>
                                    <p>{{ equipe[1].phonenumber }}<br>{{ equipe[1].mail }}</p>
                                </div>
                            </div>
                        </div>
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"{{ vich_uploader_asset(equipe[2], 'photoFile') }}\" class=\"equipe__img equipe__img--nordine\" alt=\"Nordine LAGRAGUI\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4 class=\"equipe__name\">{{ equipe[2].firstname}} {{equipe[2].lastname}}</h4>
                                    <p>{{ equipe[2].phonenumber }}<br>{{ equipe[2].mail }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"billy-sallaumines\">
                    <h3 class=\"billy-sallaumines__title align-items-baseline\">BILLY-MONTIGNY / SALLAUMINES</h3>
                    <div class=\"billy-sallaumines__content\">
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"{{ vich_uploader_asset(equipe[3], 'photoFile') }}\" class=\"equipe__img\" alt=\"Yohan SARDO\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4  class=\"equipe__name\">{{ equipe[3].firstname}} {{equipe[3].lastname}}</h4>
                                    <p>{{ equipe[3].phonenumber }}<br>{{ equipe[3].mail }}</p>
                                </div>
                            </div>
                        </div>
                        <div class=\"equipe\">
                            <div class=\"equipe__content\">
                                <div class=\"equipe__image\">
                                    <img src=\"{{ vich_uploader_asset(equipe[4], 'photoFile') }}\" class=\"equipe__img\" alt=\"Xavier HEINTZE\">
                                </div>
                                <div class=\"equipe__description\">
                                    <h4 class=\"equipe__name\">{{ equipe[4].firstname}} {{equipe[4].lastname}}</h4>
                                    <p>{{ equipe[4].phonenumber }}<br>{{ equipe[4].mail }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"secretaire\">
            <h2 class=\"secretaire__title\">SECRETAIRE COMPTABLE</h2>
            <div class=\"equipe\">
                <div class=\"equipe__content\">
                    <div class=\"equipe__image\">
                        <img src=\"{{ vich_uploader_asset(equipe[5], 'photoFile') }}\" class=\"equipe__img\" alt=\"Rabia BELAICHE\">
                    </div>
                    <div class=\"equipe__description\">
                        <h4 class=\"equipe__name\">{{ equipe[5].firstname}} {{equipe[5].lastname}}</h4>
                        <p>{{ equipe[5].phonenumber }}<br>{{ equipe[5].mail }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}", "home/equipe.html.twig", "/htdocs/templates/home/equipe.html.twig");
    }
}
