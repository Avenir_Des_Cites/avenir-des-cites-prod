<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/footer.html.twig */
class __TwigTemplate_8201b97b67ed4080788040d65ccfd0d5ecd1cf825d6282c7d2648e2e909b5770 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/footer.html.twig"));

        // line 1
        $this->displayBlock('footer', $context, $blocks);
        // line 24
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 1
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 2
        echo "
    <div class=\"reseaux\">
        <p>Retrouvez-nous sur les réseaux sociaux</p>
        <div class=\"reseaux-sociaux\">
            <a class=\"nav-item nav-link social\" href=\"https://www.facebook.com/Avenirdescites/\"><img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["reseaux"]) || array_key_exists("reseaux", $context) ? $context["reseaux"] : (function () { throw new RuntimeError('Variable "reseaux" does not exist.', 6, $this->source); })()), 0, [], "array", false, false, false, 6), "logoFile"), "html", null, true);
        echo "\" alt=\"facebook\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
            <a class=\"nav-item nav-link social\" href=\"https://twitter.com/avenirdescites\"><img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["reseaux"]) || array_key_exists("reseaux", $context) ? $context["reseaux"] : (function () { throw new RuntimeError('Variable "reseaux" does not exist.', 7, $this->source); })()), 1, [], "array", false, false, false, 7), "logoFile"), "html", null, true);
        echo "\" alt=\"twitter\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
            <a class=\"nav-item nav-link social\" href=\"https://www.youtube.com/channel/UCbv5yCOkZkDfDf83PAORARw\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["reseaux"]) || array_key_exists("reseaux", $context) ? $context["reseaux"] : (function () { throw new RuntimeError('Variable "reseaux" does not exist.', 8, $this->source); })()), 2, [], "array", false, false, false, 8), "logoFile"), "html", null, true);
        echo "\" alt=\"youtube\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
        </div>
    </div>

    <div class=\"prefooter\">
        <h2 class=\"prefooter__title\">SERVICE DE PREVENTION SPECIALISEE</h2>
    </div>    
    
    <div class=\"footer text-center justify-content-center\">
        <a class=\"nav-item nav-link mentions \" href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("mentions");
        echo "\">Mentions légales<span class=\"sr-only\"></span></a>
        <div class=\"siege-social\">
            <p class=\"siege\">";
        // line 19
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["accueil"]) || array_key_exists("accueil", $context) ? $context["accueil"] : (function () { throw new RuntimeError('Variable "accueil" does not exist.', 19, $this->source); })()), 0, [], "array", false, false, false, 19), "siegeSocial", [], "any", false, false, false, 19);
        echo "</p>
        </div>
        <p class=\"footer__copyright\">© 2020 avenirdescites.fr</p>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  88 => 19,  83 => 17,  71 => 8,  67 => 7,  63 => 6,  57 => 2,  50 => 1,  43 => 24,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block footer %}

    <div class=\"reseaux\">
        <p>Retrouvez-nous sur les réseaux sociaux</p>
        <div class=\"reseaux-sociaux\">
            <a class=\"nav-item nav-link social\" href=\"https://www.facebook.com/Avenirdescites/\"><img src=\"{{ vich_uploader_asset(reseaux[0], 'logoFile') }}\" alt=\"facebook\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
            <a class=\"nav-item nav-link social\" href=\"https://twitter.com/avenirdescites\"><img src=\"{{ vich_uploader_asset(reseaux[1], 'logoFile') }}\" alt=\"twitter\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
            <a class=\"nav-item nav-link social\" href=\"https://www.youtube.com/channel/UCbv5yCOkZkDfDf83PAORARw\"><img src=\"{{ vich_uploader_asset(reseaux[2], 'logoFile') }}\" alt=\"youtube\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
        </div>
    </div>

    <div class=\"prefooter\">
        <h2 class=\"prefooter__title\">SERVICE DE PREVENTION SPECIALISEE</h2>
    </div>    
    
    <div class=\"footer text-center justify-content-center\">
        <a class=\"nav-item nav-link mentions \" href=\"{{ path('mentions') }}\">Mentions légales<span class=\"sr-only\"></span></a>
        <div class=\"siege-social\">
            <p class=\"siege\">{{ accueil[0].siegeSocial | raw }}</p>
        </div>
        <p class=\"footer__copyright\">© 2020 avenirdescites.fr</p>
    </div>

{% endblock %} ", "partials/footer.html.twig", "/htdocs/templates/partials/footer.html.twig");
    }
}
