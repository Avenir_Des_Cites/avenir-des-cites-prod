<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/header.html.twig */
class __TwigTemplate_85ef3f68b9b4003824b9084ef133917fef1b96e5d25104fa6513629af1b8d127 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'header' => [$this, 'block_header'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/header.html.twig"));

        // line 1
        $this->displayBlock('header', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 2
        echo "
    <nav class=\"navbar navbar-expand-lg navbar-light bg-light\" id=\"top\">
    <div class=\"logo\">
        <a href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["accueil"]) || array_key_exists("accueil", $context) ? $context["accueil"] : (function () { throw new RuntimeError('Variable "accueil" does not exist.', 5, $this->source); })()), 0, [], "array", false, false, false, 5), "logoFile"), "html", null, true);
        echo "\" width=\"110\" height=\"110\" class=\"d-inline-block align-top logo\" alt=\"Logo d'Avenir des cités\" loading=\"lazy\"></a>
    </div>
    <div class=\"logo--mobile\">
        <a href=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"><img src=\"../img/A_icon.svg\" width=\"60\" height=\"60\" class=\"d-inline-block align-top logo--mobile\" alt=\"Logo d'Avenir des cités\" loading=\"lazy\"></a>
    </div>
        <div class=\"media__content\">
            <p class=\"media__reseaux text-center\">Retrouvez-nous sur les réseaux sociaux</p>
            <div class=\"media nav justify-content-around\">
            ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reseaux"]) || array_key_exists("reseaux", $context) ? $context["reseaux"] : (function () { throw new RuntimeError('Variable "reseaux" does not exist.', 13, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["reseau"]) {
            // line 14
            echo "                <a class=\"nav-item nav-link social\" href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reseau"], "url", [], "any", false, false, false, 14), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["reseau"], "logoFile"), "html", null, true);
            echo "\" alt=\"Logo des réseaux sociaux\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reseau'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "            </div>
        </div>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse mobile\" id=\"navbarNavAltMarkup\">
            <div class=\"navbar-nav mx-auto\">
                <div class=\"img__navbar\">
                    <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, (isset($context["accueil"]) || array_key_exists("accueil", $context) ? $context["accueil"] : (function () { throw new RuntimeError('Variable "accueil" does not exist.', 24, $this->source); })()), 0, [], "array", false, false, false, 24), "logoFile"), "html", null, true);
        echo "\" width=\"70\" height=\"70\" class=\"img__nav\" alt=\"Logo d'Avenir des cités\" loading=\"lazy\">
                </div>
                <a class=\"nav-item nav-link active navbar-item\" href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\">Accueil <span class=\"sr-only\">(current)</span></a>
                <a class=\"nav-item nav-link navbar-item\" href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("presentation");
        echo "\">Présentation</a>
                <a class=\"nav-item nav-link navbar-item\" href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("equipe");
        echo "\">L'équipe éducative</a>
                <a class=\"nav-item nav-link navbar-item\" href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article_index");
        echo "\">Actualité</a>
                <a class=\"nav-item nav-link navbar-item\" href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("partenaires");
        echo "\" tabindex=\"-1\">Nos partenaires</a>
            </div>
        </div>
    </nav>
    <div class=\"container-header\"></div>
    <div id=\"scrollUp\" class=\"scroll\">
        <a href=\"#top\"><img class=\"scroll__icon\" src=\"../img/chevron.svg\" alt=\"Bouton de scroll\"/></a>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "partials/header.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  121 => 30,  117 => 29,  113 => 28,  109 => 27,  105 => 26,  100 => 24,  90 => 16,  79 => 14,  75 => 13,  67 => 8,  59 => 5,  54 => 2,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block header %}

    <nav class=\"navbar navbar-expand-lg navbar-light bg-light\" id=\"top\">
    <div class=\"logo\">
        <a href=\"{{ path('home') }}\"><img src=\"{{ vich_uploader_asset(accueil[0], 'logoFile') }}\" width=\"110\" height=\"110\" class=\"d-inline-block align-top logo\" alt=\"Logo d'Avenir des cités\" loading=\"lazy\"></a>
    </div>
    <div class=\"logo--mobile\">
        <a href=\"{{ path('home') }}\"><img src=\"../img/A_icon.svg\" width=\"60\" height=\"60\" class=\"d-inline-block align-top logo--mobile\" alt=\"Logo d'Avenir des cités\" loading=\"lazy\"></a>
    </div>
        <div class=\"media__content\">
            <p class=\"media__reseaux text-center\">Retrouvez-nous sur les réseaux sociaux</p>
            <div class=\"media nav justify-content-around\">
            {% for reseau in reseaux %}
                <a class=\"nav-item nav-link social\" href=\"{{ reseau.url }}\"><img src=\"{{ vich_uploader_asset(reseau, 'logoFile') }}\" alt=\"Logo des réseaux sociaux\" width=\"30\" height=\"30\" loading=\"lazy\"></a>
            {% endfor %}
            </div>
        </div>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse mobile\" id=\"navbarNavAltMarkup\">
            <div class=\"navbar-nav mx-auto\">
                <div class=\"img__navbar\">
                    <img src=\"{{ vich_uploader_asset(accueil[0], 'logoFile') }}\" width=\"70\" height=\"70\" class=\"img__nav\" alt=\"Logo d'Avenir des cités\" loading=\"lazy\">
                </div>
                <a class=\"nav-item nav-link active navbar-item\" href=\"{{ path('home') }}\">Accueil <span class=\"sr-only\">(current)</span></a>
                <a class=\"nav-item nav-link navbar-item\" href=\"{{ path('presentation') }}\">Présentation</a>
                <a class=\"nav-item nav-link navbar-item\" href=\"{{ path('equipe') }}\">L'équipe éducative</a>
                <a class=\"nav-item nav-link navbar-item\" href=\"{{ path('article_index') }}\">Actualité</a>
                <a class=\"nav-item nav-link navbar-item\" href=\"{{ path('partenaires') }}\" tabindex=\"-1\">Nos partenaires</a>
            </div>
        </div>
    </nav>
    <div class=\"container-header\"></div>
    <div id=\"scrollUp\" class=\"scroll\">
        <a href=\"#top\"><img class=\"scroll__icon\" src=\"../img/chevron.svg\" alt=\"Bouton de scroll\"/></a>
    </div>

{% endblock %}", "partials/header.html.twig", "/htdocs/templates/partials/header.html.twig");
    }
}
