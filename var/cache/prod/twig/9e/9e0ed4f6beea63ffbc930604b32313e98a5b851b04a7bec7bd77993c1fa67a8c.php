<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/articles.html.twig */
class __TwigTemplate_92a598c29b8d5c3af65f0a9dd38bb4f370cd4b47eee0e719ecb564952157e3e5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/articles.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/articles.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Articles";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"container articles\">
        <div class=\"articles__block\">
        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 9, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 10
            echo "            <article class=\"article\">
                <div class=\"article__image\">
                    <img src=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["article"], "illuFile"), "html", null, true);
            echo "\" alt=\"Photo de l'article\">
                </div>
                <p class=\"article__publication\"><i class=\"visible fas fa-clock\"></i> ";
            // line 14
            ((twig_get_attribute($this->env, $this->source, $context["article"], "publication", [], "any", false, false, false, 14)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "publication", [], "any", false, false, false, 14), "d-m-Y"), "html", null, true))) : (print ("")));
            echo "</p>
                <div class=\"article__description\">
                    <p>";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "resume", [], "any", false, false, false, 16), "html", null, true);
            echo "</p>
                </div>
                <div class=\"article__button\"><a class=\"article__button--link\" href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article_show", ["id" => twig_get_attribute($this->env, $this->source, $context["article"], "id", [], "any", false, false, false, 18)]), "html", null, true);
            echo "\">LIRE LA SUITE <i class=\"visible fas fa-arrow-right\"></i></a></div>
            </article>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 21
            echo "            <div>
                <p>Il n'y a aucun article pour le moment, repassez plus tard pour plus d'actualité !</p>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "        <div class=\"navigation\">
            ";
        // line 26
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 26, $this->source); })()));
        echo "
        </div>
        </div>
       
        <aside class=\"aside\">
            <h2 class=\"aside__title\">Retrouvez notre prochain événement :</h2>
            <div class=\"aside__content\">
            ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["events"]) || array_key_exists("events", $context) ? $context["events"] : (function () { throw new RuntimeError('Variable "events" does not exist.', 33, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
            // line 34
            echo "            <p class=\"aside__content--name\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "name", [], "any", false, false, false, 34), "html", null, true);
            echo "</p>
            <div class=\"aside__content--infos\">
                <div>";
            // line 36
            ((twig_get_attribute($this->env, $this->source, $context["event"], "date", [], "any", false, false, false, 36)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "date", [], "any", false, false, false, 36), "d-m-Y"), "html", null, true))) : (print ("")));
            echo "</div>
                <div>";
            // line 37
            ((twig_get_attribute($this->env, $this->source, $context["event"], "time", [], "any", false, false, false, 37)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "time", [], "any", false, false, false, 37), "H:i:s"), "html", null, true))) : (print ("")));
            echo "</div>
                <div>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "place", [], "any", false, false, false, 38), "html", null, true);
            echo "</div>
            </div>
            <img class=\"aside__content--img\" src=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["event"], "illuFile"), "html", null, true);
            echo "\" alt=\"Photo de l'événement\"/>
            <div class=\"aside__content--description\">";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "description", [], "any", false, false, false, 41), "html", null, true);
            echo "</div>
            
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "            </div>
        </aside>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/articles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 44,  159 => 41,  155 => 40,  150 => 38,  146 => 37,  142 => 36,  136 => 34,  132 => 33,  122 => 26,  119 => 25,  110 => 21,  102 => 18,  97 => 16,  92 => 14,  87 => 12,  83 => 10,  78 => 9,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Articles{% endblock %}

{% block body %}

    <div class=\"container articles\">
        <div class=\"articles__block\">
        {% for article in pagination %}
            <article class=\"article\">
                <div class=\"article__image\">
                    <img src=\"{{ vich_uploader_asset(article, 'illuFile') }}\" alt=\"Photo de l'article\">
                </div>
                <p class=\"article__publication\"><i class=\"visible fas fa-clock\"></i> {{ article.publication ? article.publication|date('d-m-Y') : '' }}</p>
                <div class=\"article__description\">
                    <p>{{ article.resume }}</p>
                </div>
                <div class=\"article__button\"><a class=\"article__button--link\" href=\"{{ path('article_show', {'id': article.id}) }}\">LIRE LA SUITE <i class=\"visible fas fa-arrow-right\"></i></a></div>
            </article>
        {% else %}
            <div>
                <p>Il n'y a aucun article pour le moment, repassez plus tard pour plus d'actualité !</p>
            </div>
        {% endfor %}
        <div class=\"navigation\">
            {{ knp_pagination_render(pagination) }}
        </div>
        </div>
       
        <aside class=\"aside\">
            <h2 class=\"aside__title\">Retrouvez notre prochain événement :</h2>
            <div class=\"aside__content\">
            {% for event in events %}
            <p class=\"aside__content--name\">{{ event.name }}</p>
            <div class=\"aside__content--infos\">
                <div>{{ event.date ? event.date|date('d-m-Y') : '' }}</div>
                <div>{{event.time ? event.time|date('H:i:s') : ''}}</div>
                <div>{{event.place}}</div>
            </div>
            <img class=\"aside__content--img\" src=\"{{ vich_uploader_asset(event, 'illuFile') }}\" alt=\"Photo de l'événement\"/>
            <div class=\"aside__content--description\">{{ event.description}}</div>
            
            {% endfor %}
            </div>
        </aside>
    </div>

{% endblock %}
", "home/articles.html.twig", "/htdocs/templates/home/articles.html.twig");
    }
}
